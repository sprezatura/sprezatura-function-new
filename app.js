'use strict';

require('module-alias/register');
const admin = require('@db').admin;
const db = require('@db').db;
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const bodyParser = require('body-parser')
const axios = require('axios')
const FormData = require('form-data');
const http = require('https');
const querystring = require('querystring');
const app = express();
const mail = require('./src/mail');

function recaptchaSiteverify(token, isMobile, cb) {
    let _secret = '';
    if(isMobile) {
        _secret =  '6LdzeacUAAAAAP8jaF4h3Ph1YwxEsODXDdmTE-EE'; // android
    } else {
        _secret = '6LewR6QUAAAAAL_xIJZUQcYiDnguPUx-Q5hO8fUc'; // web
    }
    // http data.
    const data = querystring.stringify({
        'secret': _secret,
        'response': token,
    });
    // http option.
    const option = {
        host: 'www.google.com',
        port: '443',
        path: '/recaptcha/api/siteverify',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(data)
        }
    };
    const rq = http.request(option, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (data) {
            cb(data);
        });
    });
    // http request.
    rq.write(data);
    rq.end();
}



app.use(bodyParser());
app.use(cookieParser())
app.use(cors({ origin: true, credentials: true, methods: ['GET', 'POST'] }))

app.get('/', (req, res) => {
    res.status(200).send(`Hello, world!`);
});



app.post('/signUp', (request, response) => {
    console.log(request);

    let _email = request.body.email;
    let _password = request.body.password;
    let _firstName = request.body.firstName;
    let _lastName = request.body.lastName;
    let _displayName = _firstName + _lastName;
    let _isMobile = request.body.mobile;

    recaptchaSiteverify(request.body.reToken, _isMobile, function (data) {
        let d = JSON.parse(data);
        if(d.success) {
            admin.auth().createUser({
                email: _email,              // 사용자의 새 기본 이메일. 유효한 이메일 주소를 입력하십시오.
                emailVerified: false,       // 사용자의 기본 이메일이 확인되었는지 여부. 제공하지 않은 경우 기본값은 false입니다.
                // phoneNumber: null,       // 사용자의 새 기본 전화번호입니다. 유효한 E.164 사양과 호환되는 전화번호여야 합니다. 사용자의 기존 전화번호를 지우려면 null로 설정합니다.
                password: _password,        // 사용자의 해시되지 않은 새 원시 비밀번호. 6자(영문 기준) 이상이어야 합니다.
                displayName: _displayName,  // 사용자의 새 표시 이름. 사용자의 기존 표시 이름을 지우려면 null로 설정합니다.
                photoURL: "http://www.example.com/12345678/photo.png",            // 사용자의 새 사진 URL. 사용자의 기존 사진 URL을 지우려면 null로 설정합니다. null이 아닌 경우 유효한 URL을 제공해야 합니다.
                disabled: false             // 사용자가 중지되었는지 여부. 중지된 경우 true, 사용 설정된 경우 false.
            })
            .then ((userRecord) => {
                if (! userRecord.email) {
                    response.send(JSON.stringify({ success: true }));
                    return;
                }
                mail.sendVerifyEmail(userRecord.email);
                response.send(JSON.stringify({ success: true }));
            }).catch(error => {
                console.log("error explain", error);
                response.send(JSON.stringify({ success: false }));
            });     
        } else {
            response.end(JSON.stringify({ success: false }));
            return true;
        }
    });
});


app.post('/sendVerifyEmail', (request, response) => {
    mail.sendVerifyEmail(request.body.targetEmail).then(result => {
        console.log(result);
        response.end(JSON.stringify({ success: true }));
    })
});


app.post('/createSession', (request, response) => {
    let idToken = request.body._idToken; // 발급 아이디 토큰
    console.log(idToken);
    const expiresIn = 60 * 60 * 24 * 5 * 1000; // 쿠키 생존 시간.
    admin.auth().createSessionCookie(idToken, { expiresIn }).then((sessionCookie) => {
        const options = { maxAge: expiresIn, httpOnly: true, path: '/', secure: false, domain: false }; //dev code
        // const options = {maxAge: expiresIn, httpOnly: true, secure: true, domain: ".sprezatura.net" }; // production.
        response.setHeader('Cache-Control', 'private');
        response.cookie('__session', sessionCookie, options);
        response.end(JSON.stringify({ success: true, sessionCookie }));
        return true;
    }).catch(error => {
        console.log(error);
        response.status(401).send('UNAUTHORIZED REQUEST!');
        return true;
    });
});

app.get('/verifyToken', (request, response) => {
    const sessionCookie = request.cookies.__session || '';
    if(!sessionCookie) response.end(JSON.stringify({ "success": false }));
    
    admin.auth().verifySessionCookie(sessionCookie, true).then((decodedClaims) => {
        let uid = decodedClaims.uid;
        console.log(decodedClaims);
        return admin.auth().createCustomToken(uid);
    }).then(customToken => {
        response.end(JSON.stringify({ "success": true, "token": customToken }));
        return true;
    }).catch(error => {
        switch(error.code)
        {
            case 'auth/session-cookie-revoked':
            response.send(400, JSON.stringify({ "success": false }));
            break;

            default:
            response.end(JSON.stringify({ "success": false }));
            break;
        }
        return true;
    })
});


// 링크 초대가 유효한지 확인합니다.
app.post ('/verifyInvitingMember', (request, response) => {
    const sessionCookie = request.cookies.__session || '';
    const { workspaceId, memberInvitingId, pinCode } = request.body.data;


    return admin.auth ().verifySessionCookie (sessionCookie, true).then ((decodedClaims) => {
        return admin.auth().getUser(decodedClaims.uid);
    })


    // Get User Data
    .then ((userRecord) => {
        const { uid, email, displayName, phoneNumber, photoURL } = userRecord;
        return { uid, email, displayName, phoneNumber, photoURL };
    })


    // Run Transaction
    .then ((userData) => {
        console.log ("invite");
        console.log (userData);

        return db.runTransaction ((transaction) => {
            const _workspaceDocRef = db.collection ("workspace").doc (workspaceId);
            

            // Get Workspace Document, Verify - 사용 중인 워크 스페이스 정보를 가져옵니다. 
            return transaction.get (_workspaceDocRef).then((workspaceDoc) => {
                if (! workspaceDoc.exists) {
                    throw new Error ("Error Internal");
                }


                const _workspaceDocData = workspaceDoc.data ();

                // 사용자가 특정 워크 스페이스의 동료로 속해 있는지 확인합니다.
                if (_workspaceDocData.memberUidList.includes (userData.uid)) {
                    throw new Error ("Exist Include Workspace");
                }


                // 연결된 구독이 현재 사용할 수 있는지 확인합니다.
                const { expiryDate, activeDate }  = _workspaceDocData.subscriptionItem.data,
                _currentDate = new Date ();

                if (_currentDate >= expiryDate || _currentDate < activeDate) {
                    throw new Error ("Error Internal");
                } 


                return { 
                    id: workspaceDoc.id,
                    data: _workspaceDocData,
                };
            })


            // Verify Creating Member Document - 새로운 멤버를 생성할 수 있는지 확인합니다.
            .then ((workspaceItem) => {
                const _memberCurrentSize = workspaceItem.data.size.member,
                _subscriptionMemberLimitOption = workspaceItem.data.subscriptionItem.data.limitOption.member;


                if (_memberCurrentSize >= _subscriptionMemberLimitOption) {
                    throw new Error ("Error Internal");
                }

                return workspaceItem;
            })


            // Verify Member Inviting Document
            .then ((workspaceItem) => {
                const _memberInvitingDocRef = _workspaceDocRef.collection ("memberInviting").doc (memberInvitingId);


                return transaction.get (_memberInvitingDocRef).then ((memberInvitingDoc) => {
                    if (! memberInvitingDoc.exists) {
                        throw new Error ("Error Internal");
                    }

                    const _memberInvitingDocData = memberInvitingDoc.data ();

                    if (_memberInvitingDocData.pinCode !== pinCode) {
                        throw new Error ("Error Internal");
                    }


                    return workspaceItem;
                });
            })


            // Create New Member Document
            .then ((workspaceItem) => {
                const _memberDocRef = _workspaceDocRef.collection ("member").doc (),
                { uid, email, displayName, phoneNumber, photoURL } = userData; 
    
                const _memberDocData = {
                    email: email,
                    uid: uid,
                    displayName: displayName === undefined ? "" : displayName,
                    phoneNumber: phoneNumber === undefined ? "" : phoneNumber,
                    photoURL: photoURL === undefined ? "" : photoURL,
                    job: "",
                    message: "",
                    status: "online",
                    schedule: {
                        workingDate: {
                            mon: true,
                            tue: true,
                            wed: true,
                            thu: true,
                            fri: true,
                            sat: false,
                            sun: false,
                        },
                        workingTime: {
                            startingTime: "09:00",
                            endingTime: "18:00",
                        },
                        oneDay: null
                    }
                };

                transaction.set (_memberDocRef, _memberDocData);
                return workspaceItem;
            })


            // Update Workspace Document
            .then ((workspaceItem) => {
                const _memberUidList = workspaceItem.data.memberUidList;
                _memberUidList.push (userData.uid);


                transaction.update (_workspaceDocRef, {
                    "issue.member": workspaceItem.data.issue.member + 1,
                    "size.member": workspaceItem.data.size.member + 1,
                    "memberUidList": _memberUidList,
                });


                return workspaceItem;
            });
        });

    })


    // Success Result
    .then (() => {
        response.end (JSON.stringify ({ "success": true }));
        return true;
    })


    // Error Result
    .catch((error) => {
        response.end (JSON.stringify ({ "success": false, message: error.message }));
        return true;
    });

});


// User Document Email을 수정합니다.
app.post ("/updateUserDocumentEmail", (request, response) => {
    const sessionCookie = request.cookies.__session || '';


    // Get User Data
    return admin.auth ().verifySessionCookie (sessionCookie, true).then ((decodedClaims) => {
        return admin.auth().getUser(decodedClaims.uid);
    })
    .then ((userRecord) => {
        const { uid, email, displayName, phoneNumber, photoURL } = userRecord;
        return { uid, email, displayName, phoneNumber, photoURL };
    })


    // // Run Update User Document Transaction
    .then ((userData) => {
        const _userDocRef = db.collection ("user").doc (userData.uid);
        _userDocRef.update ({ email: userData.email });


        // Run Transaction
        return db.runTransaction((transaction) => {
            return transaction.get (_userDocRef).get ().then ((userDoc) => {
                if (! userDoc.exists) {
                    transaction.set (_userDocRef, {
                        email: userData.email ? userData.email : "",
                        displayName: userData.displayName ? userData.displayName : "",
                        phoneNumber: userData.phoneNumber ? userData.phoneNumber : "",
                        photoURL: userData.photoURL ? userData.photoURL : "",
                        uid: userData.uid,
                    });

                    return true;
                }


                transaction.update (_userDocRef, { email: email });
                return true;
            });
        });
    })
    .then (() => {
        response.end (JSON.stringify ({ "success": true }));
    })
    .catch ((error) => {
        console.log (error);
        response.end (JSON.stringify ({ "success": false }));
    })
});


// 이미 초대된 멤버들 중 사용자가 있는지 검사후 정보를 갱신합니다.
app.post ("/enterInvitedWorkspace", (request, response) => {
    const sessionCookie = request.cookies.__session || '';

    // Get User Data
    return admin.auth ().verifySessionCookie (sessionCookie, true).then ((decodedClaims) => {
        return admin.auth().getUser(decodedClaims.uid);
    })
    .then ((userRecord) => {
        const { uid, email, displayName, phoneNumber, photoURL } = userRecord;
        return { uid, email, displayName, phoneNumber, photoURL };
    })


    // Run Transaction Update Member Data
    .then ((userData) => {
        if (! userData.email) { return }
        
        const _memberInvitingQueueDocRef = admin.firestore ().collection ("memberInvitingQueue").doc (userData.email.replace (".", "*"));


        // Run Transaction Processing.
        return admin.firestore ().runTransaction ((transaction) => {
            // Get Transaction
            return transaction.get (_memberInvitingQueueDocRef).then((memberInvitingQueueDoc) => {
                if (! memberInvitingQueueDoc.exists) {
                    return true;
                }


                // Update Member Doc List
                const _queueObject = memberInvitingQueueDoc.data ().queue,
                _workspaceIdList =  Object.keys (_queueObject);

                
                _workspaceIdList.map ((workspaceId) => {
                    const _memberId = _queueObject[workspaceId],
                    _workspaceDocRef =  admin.firestore ().collection ("workspace").doc (workspaceId),
                    _memberDocRef = _workspaceDocRef.collection ("member").doc (_memberId);


                    // Transaction Update
                    transaction.update (_workspaceDocRef, {
                        memberUidList:  admin.firestore.FieldValue.arrayUnion (userData.uid),
                    });

                    transaction.update (_memberDocRef, {
                        uid: userData.uid,
                        displayName: userData.displayName ? userData.displayName : "",
                        phoneNumber: userData.phoneNumber ? user.phoneNumber : "",
                        photoURL: userData.photoURL ? userData.photoURL : "",
                        status: "online",
                    });


                    return true;
                });


                // Delete Member Inviting Queue Doc Ref
                transaction.delete (_memberInvitingQueueDocRef);
                return true;
            });
        });
    })


    // Result (Success, Fail)
    .then (() => {
        response.end (JSON.stringify ({ "success": true }));
        return true;
    })
    .catch ((error) => {
        console.log(error.message);

        response.end (JSON.stringify ({ "success": false }));
        return true;
    })
});


if (module === require.main) {
    // [START server]
    // Start the server
    const server = app.listen(process.env.PORT || 12000, () => {
        const port = server.address().port;
        console.log(`App listening on port ${port}`);
    });
    // [END server]
}

module.exports = app;
