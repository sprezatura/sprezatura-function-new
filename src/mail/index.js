const admin = require('@db').admin;
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendVerifyEmail = function (targetEmail) { 
    console.log('send mail');
    const actionCodeSettings = {
        url: 'http://localhost',
        handleCodeInApp: false,
    };
    return admin.auth().generateEmailVerificationLink(targetEmail, actionCodeSettings).then( link => {
        const emailContent = {
            to: targetEmail,
            from: 'support@sprezatura.net',
            subject:  '[Sprezatura] 이메일 인증을 완료해주세요. - sprezatura.net',
            text: '스프레차투라 이메일 인증을 완료해주세요.',
            html: `
            <div style="border: 1px solid #CFCFCF; margin: 60px; max-width: 560px;">
                <div class="logo" style="text-align: center">
                    <img style="margin: 32px 0" height="36" src="https://storage.googleapis.com/sprezatura-app.appspot.com/image/sprezatura_logo_72.png" />
                </div>  
                <div class="header" style="padding: 24px 32px; background: #0167BF;">
                    <h3 style="line-height: 14px; font-size: 14px; color: #fff">스프레차투라</h3>
                    <h1 style="line-height: 18px; font-size: 18px; color: #fff">이메일 인증을 완료해주세요</h1>
                </div>
                <div class="content" style="padding: 32px;">
                    <p style="line-height: 24px; font-size: 14px; color: #0167BF">당신이 사랑하는 일에 스프레차투라가 쓰여질 수 있게 된 것에 진심으로 감사드립니다. 변함 없이 최고의 품질로 서비스를 제공하기 위해서 최선을 다하도록하겠습니다.</p>
                    <a href="${link}" style="text-decoration: none;">
                    <div class="button" style="margin: 24px 0; display: inline-block; vertical-align: middle; padding: 20px 28px; font-size: 18px; line-height: 18px; color: #fff; background: #0167BF; border-radius: 999px;">
                        <span style="font-size: 18px; line-height: 18px;">이메일 인증</span>
                    </div>
                    </a>
                    <p style="line-height: 24px; font-size: 14px; color: #818181;">혹시 이메일 인증 요청을 하신적이 없으신가요?  그렇다면 이 이메일을 무시하셔도 됩니다. 지속적으로 당신을 불편하게 한다면 support@xharpen.com으로 서비스팀이 조취를 취할 수 있도록 이메일을 보내주세요.</p>
                    <p style="margin-top: 24px; line-height: 24px; font-size: 14px; color: #818181;">오늘도 기분 좋은 하루 되시길 바랍니다.</p>
                    <p style="margin-top: 24px; line-height: 24px; font-size: 14px; color: #818181;">스프레차투라 팀 드림.</p>
                </div>
            </div>`,
        };
        console.log(targetEmail, link);
        return sgMail.send(emailContent);
    })
}

module.exports = {
    sendVerifyEmail,
}