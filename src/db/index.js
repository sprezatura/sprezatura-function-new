const config = require('@config');
const admin = require('firebase-admin');
const serviceAccount = require("./google-services.json");

// Firebase 기본 설정
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://sprezatura-app.firebaseio.com'
});
const settings = { timestampsInSnapshots: true };
admin.firestore().settings(settings)

// Firestore Database 사용
const db = admin.firestore();

module.exports = {
    admin,
    db
}